var gulp = require('gulp'),
	sass = require('gulp-sass'),
	inky = require('inky'),
	inlineCss = require('gulp-inline-css'),
	inlineSource = require('gulp-inline-source'),
	browserSync = require('browser-sync').create();

gulp.task('server', function() {
	browserSync.init({
		server: "./dist",
		port: 4000
	});
});

//STYLES
gulp.task('styles', function () {
	return gulp.src('./scss/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./css'))
		browserSync.reload();
});

//CONVERTE INKY
gulp.task('inky', function () {
	return gulp.src('./templates/**/*.html')
		.pipe(inlineSource())
		.pipe(inky())
		.pipe(inlineCss({
			preserveMediaQueries: true,
			removeLinkTags: false
		}))
		.pipe(gulp.dest('./dist'));
});

//WATCH
gulp.task('watch-server', gulp.series(['styles', 'inky']), function () {
	browserSync.reload();
});

gulp.task('watch', function () {
	gulp.watch(['./scss/**/*.scss', './templates/**/*.html'], gulp.series(['watch-server']));
});

// gulp.task('default',
// 	gulp.series(
// 		"server",
// 		"watch"
// 	)
// );

gulp.task('default', gulp.series(
	gulp.parallel('watch', 'server')
));
