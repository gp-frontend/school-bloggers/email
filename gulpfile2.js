var gulp = require('gulp'),
		sass = require('gulp-sass'),
		inky = require('inky'),
		inlineCss = require('gulp-inline-css'),
		inlinesource = require('gulp-inline-source'),
		browserSync = require('browser-sync').create();

gulp.task('server', ['styles', 'inky'], function() {
	browserSync.init({
		server: "./dist",
		port: 4000
	});
});

//STYLES
gulp.task('styles', function () {
	return gulp.src('./scss/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./css'))
		browserSync.reload();
});

//CONVERTE INKY
gulp.task('inky', ['styles'], function() {
	return gulp.src('./templates/**/*.html')
		.pipe(inlinesource())
		.pipe(inky())
		.pipe(inlineCss({
				preserveMediaQueries: true,
				removeLinkTags: false
		}))
		.pipe(gulp.dest('./dist'));
});


gulp.task('watch-server', ['styles', 'inky'], function () {
	browserSync.reload();
});

//WATCH
gulp.task('watch', function() {
	gulp.watch(['./scss/**/*.scss'],['watch-server']);
	gulp.watch(['./templates/**/*.html'],['watch-server']);
});

gulp.task('default', ['watch','server']);
